<!DOCTYPE html>
<html lang="en">
   <head>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="icon" type="image/png" href="assets/images/watson_avatar.jpg"/> 
      <title><?php (!empty($page_title)) ? print $page_title : print 'AppChattbot';?></title>
      
      <!-- Bootstrap Core CSS -->
      <link href="<?php echo $relative_path;?>assets/css/bootstrap.min.css" rel="stylesheet">

      <!-- Custom CSS -->
      <link href="<?php echo $relative_path;?>assets/css/style.css" rel="stylesheet">
   </head>
   <body>